within QuasiStatic_DeepBar.Examples.BasicMachines.InductionMachines;
model IMC_Characteristics_no_deepbar "Characteristic curves of Induction machine with squirrel cage"
  extends Modelica.Icons.Example;
  import Modelica.Constants.pi;
  final parameter Integer m = 3 "Number of phases" annotation (
    Evaluate = true);
  parameter Modelica.Units.SI.Voltage VsNominal = 100 "Nominal RMS voltage per phase";
  parameter Modelica.Units.SI.Current IsNominal = 100 "Nominal RMS current per phase";
  parameter Modelica.Units.SI.Frequency fsNominal = 50 "Nominal frequency";
  parameter Modelica.Units.SI.AngularVelocity w_Load(displayUnit = "rev/min") = 1440.45*2*Modelica.Constants.pi/60 "Nominal load speed";
  parameter Integer p = 2 "Number of pole pairs";
  Real speedPerUnit = p*imcQS.wMechanical/(2*pi*fsNominal) "Per unit speed";
  Real slip = 1 - speedPerUnit "Slip";
  output Modelica.Units.SI.Current Iqs = iSensorQS.I "QS RMS current";
  Modelica.Electrical.QuasiStatic.Polyphase.Sources.VoltageSource vSourceQS(m = m, f = fsNominal, V = fill(VsNominal, m), phi = -Modelica.Electrical.Polyphase.Functions.symmetricOrientation(m)) annotation (
    Placement(transformation(origin = {-60, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 270)));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.Star starQS(m = m) annotation (
    Placement(transformation(origin = {-70, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground groundQS annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 270, origin = {-90, 20})));
  Modelica.Electrical.QuasiStatic.Polyphase.Sensors.PowerSensor pSensorQS(m = m) annotation (
    Placement(transformation(extent = {{-40, 70}, {-20, 90}})));
  Modelica.Electrical.QuasiStatic.Polyphase.Sensors.CurrentQuasiRMSSensor iSensorQS(m = m) annotation (
    Placement(transformation(extent = {{-10, 70}, {10, 90}})));
  Modelica.Magnetic.QuasiStatic.FundamentalWave.BasicMachines.InductionMachines.IM_SquirrelCage imcQS(Jr = 0,
    Lssigma=0,
    Lm=3*sqrt(1 - 0.0667)/(2*pi*50),                                                                          Lrsigma = 6*(1 - sqrt(1 - 0.0667))/(2*pi*50), Rr = 0.04, Rs=0, fsNominal = fsNominal, gamma(displayUnit = "rad"), gammar(fixed = true, start = -pi/2), gammas(displayUnit = "rad"), m = 3, p = p, wMechanical(displayUnit = "rad/s")) annotation (
    Placement(transformation(extent = {{20, 30}, {40, 50}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground groundMachineQS annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, origin = {-10, 10})));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.Star starMachineQS(m = Modelica.Electrical.Polyphase.Functions.numberOfSymmetricBaseSystems(m)) annotation (
    Placement(transformation(extent = {{-10, 10}, {10, -10}}, rotation = 270, origin = {-10, 30})));
  Modelica.Magnetic.QuasiStatic.FundamentalWave.Utilities.MultiTerminalBox terminalBoxQS(m = m, terminalConnection = "Y") annotation (
    Placement(transformation(extent = {{20, 46}, {40, 66}})));
  Modelica.Mechanics.Rotational.Sources.Speed speed(exact = true) annotation (
    Placement(transformation(extent = {{70, 30}, {50, 50}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=2*pi*fsNominal/p,                                     duration = 1,
    offset=0)                                                                                              annotation (
    Placement(transformation(extent = {{100, 30}, {80, 50}})));
equation
  connect(groundQS.pin, starQS.pin_n) annotation (
    Line(points = {{-80, 20}, {-80, 20}}, color = {85, 170, 255}));
  connect(starQS.plug_p, vSourceQS.plug_n) annotation (
    Line(points = {{-60, 20}, {-60, 30}}, color = {85, 170, 255}));
  connect(pSensorQS.currentN, iSensorQS.plug_p) annotation (
    Line(points = {{-20, 80}, {-10, 80}}, color = {85, 170, 255}));
  connect(pSensorQS.voltageP, pSensorQS.currentP) annotation (
    Line(points = {{-30, 90}, {-40, 90}, {-40, 80}}, color = {85, 170, 255}));
  connect(pSensorQS.voltageN, starQS.plug_p) annotation (
    Line(points = {{-30, 70}, {-30, 20}, {-60, 20}}, color = {85, 170, 255}));
  connect(terminalBoxQS.plug_sn, imcQS.plug_sn) annotation (
    Line(points = {{24, 50}, {24, 50}}, color = {85, 170, 255}));
  connect(terminalBoxQS.plug_sp, imcQS.plug_sp) annotation (
    Line(points = {{36, 50}, {36, 50}}, color = {85, 170, 255}));
  connect(starMachineQS.plug_p, terminalBoxQS.starpoint) annotation (
    Line(points = {{-10, 40}, {-10, 52}, {20, 52}}, color = {85, 170, 255}));
  connect(iSensorQS.plug_n, terminalBoxQS.plugSupply) annotation (
    Line(points = {{10, 80}, {30, 80}, {30, 52}}, color = {85, 170, 255}));
  connect(starMachineQS.pin_n, groundMachineQS.pin) annotation (
    Line(points = {{-10, 20}, {-10, 20}}, color = {85, 170, 255}));
  connect(imcQS.flange, speed.flange) annotation (
    Line(points = {{40, 40}, {50, 40}}));
  connect(ramp.y, speed.w_ref) annotation (
    Line(points = {{79, 40}, {72, 40}}, color = {0, 0, 127}));
  connect(vSourceQS.plug_p, pSensorQS.currentP) annotation (
    Line(points = {{-60, 50}, {-60, 80}, {-40, 80}}, color = {85, 170, 255}));
  annotation (
    experiment(StopTime = 1, Interval = 0.001, Tolerance = 1E-6),
    Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Text(extent = {{20, 8}, {100, 0}}, textString = "%m phase quasi-static", textStyle = {TextStyle.Bold})}),
    Documentation(info = "<html>
<p>
This examples allows the investigation of characteristic curves of quasi-static polyphase induction machines with squirrel cage rotor
as a function of rotor speed.
</p>

<p>
Simulate for 1 second and plot (versus <code>imcQS.wMechanical</code> or <code>speedPerUnit</code>):
</p>

<ul>
<li><code>iSensorQS.I</code>: (equivalent) RMS stator current</li>
<li><code>imcQS.tauElectrical</code>: machine torque</li>
<li><code>imcQS.powerBalance.powerStator</code>: stator power</li>
<li><code>imcQS.powerBalance.powerMechanical</code>: mechanical power</li>
</ul>
</html>"));
end IMC_Characteristics_no_deepbar;
