within QuasiStatic_DeepBar.Examples.BasicMachines.InductionMachines;
model IMC_Characteristics_file_doublecage
  import Modelica.Constants.pi;
  extends IMC_Characteristics_file_standard(imcQS(Lrsigmavar=lsigma*dX*6*(1 -
          sqrt(1 - 0.0667))/(2*pi*50), fileName=
          Modelica.Utilities.Files.loadResource("modelica://QuasiStatic_DeepBar/Resources/deepbar/doublecage.txt")));
  // Parameters determined by Julia calculation
  parameter Modelica.Units.SI.Inductance Ldc_standard = 1.932718132407955e-6;
  parameter Modelica.Units.SI.Inductance Ldc_doublecage = 4.135809265433974e-6;
  final parameter Real lsigma = Ldc_doublecage / Ldc_standard "Relative leakage inductance change";
  annotation(experiment(StopTime = 1, Interval = 0.001, Tolerance = 1E-6));
end IMC_Characteristics_file_doublecage;
