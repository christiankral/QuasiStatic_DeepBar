within QuasiStatic_DeepBar.BasicMachines.Components;
model SymmetricPolyphaseCageWinding "Symmetrical rotor cage"
  import Modelica.Constants.pi;
  import Modelica.Constants.eps;
  import Modelica.Constants.mu_0;
  extends
    Modelica.Magnetic.QuasiStatic.FundamentalWave.Interfaces.TwoPortExtended;
  parameter Integer m=3 "Number of phases" annotation(Evaluate=true);
  parameter Boolean useHeatPort=false
    "Enable / disable (=fixed temperatures) thermal port"
    annotation (Evaluate=true);
  parameter Modelica.Units.SI.Resistance RRef
    "Winding resistance per phase at TRef";
  parameter Modelica.Units.SI.Temperature TRef(start=293.15)
    "Reference temperature of winding";
  parameter
    Modelica.Electrical.Machines.Thermal.LinearTemperatureCoefficient20
    alpha20(start=0) "Temperature coefficient of winding at 20 degC";
  final parameter Modelica.Units.SI.LinearTemperatureCoefficient alphaRef=
      Modelica.Electrical.Machines.Thermal.convertAlpha(
      alpha20,
      TRef,
      293.15) "Temperature coefficient of winding at reference temperature";
  parameter Modelica.Units.SI.Temperature TOperational(start=293.15)
    "Operational temperature of winding"
    annotation (Dialog(enable=not useHeatPort));
  parameter Modelica.Units.SI.Inductance Lsigma "Cage stray inductance";
  parameter Real effectiveTurns=1 "Effective number of turns";
  final parameter Integer nBase=Modelica.Electrical.Polyphase.Functions.numberOfSymmetricBaseSystems(m)
    "Number of base systems";
  Modelica.Units.SI.ComplexCurrent i[m]=electroMagneticConverter.i
    "Cage currents";
  parameter Modelica.Units.SI.Length h = 50E-3 "Height of rotor bar";
  parameter Modelica.Units.SI.Conductivity rho = 3E-8 "Conductivity of rotor material";
  parameter Real dR = 0.7 "Fraction of deep bar resistance";
  parameter Real dX = 0.7 "Fraction of deep bar reactance";
  Real xi "Equivalent bar height";
  Real kR "Resistance increase due to deep bar effect";
  Real kX "Reactuve increase due to deep bar effect";

  Modelica.Magnetic.QuasiStatic.FundamentalWave.Components.PolyphaseElectroMagneticConverter
    electroMagneticConverter(final m=m, final effectiveTurns=effectiveTurns)
    "Symmetric winding" annotation (Placement(transformation(
        origin={0,-10},
        extent={{-10,-10},{10,10}},
        rotation=90)));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.VariableResistor
                                                           resistor(
    final useHeatPort=useHeatPort,
    final m=m,
    final T_ref=fill(TRef, m),
    final T=fill(TOperational, m),
    alpha_ref=fill(0, m))        annotation (Placement(transformation(
        origin={-20,-50},
        extent={{10,10},{-10,-10}},
        rotation=90)));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.Star star(final m=
        nBase)
           annotation (Placement(transformation(extent={{50,-30},{70,-10}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground ground
    annotation (Placement(transformation(
        origin={90,-20},
        extent={{-10,10},{10,-10}},
        rotation=270)));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a heatPortWinding
 if useHeatPort "Heat ports of winding resistor"
    annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));
  Modelica.Thermal.HeatTransfer.Components.ThermalCollector
    thermalCollector(final m=m) if useHeatPort
    "Connector of thermal rotor resistance heat ports"
    annotation (Placement(transformation(extent={{-50,-90},{-30,-70}})));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.MultiStar multiStar(final m=m)
    annotation (Placement(transformation(extent={{20,-30},{40,-10}})));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.VariableInductor
                                                           inductor(m=m)
                                                                    annotation (Placement(transformation(
        origin={0,-60},
        extent={{10,-10},{-10,10}},
        rotation=180)));
  Modelica.Blocks.Sources.RealExpression kR_Expression[m](y=fill(RRef*(1 - dR +
        dR*kR), m))
    annotation (Placement(transformation(extent={{60,-60},{40,-40}})));
  Modelica.Blocks.Sources.RealExpression kX_Expression[m](y=fill(Lsigma*(1 - dX +
        dX*kX), m))
    annotation (Placement(transformation(extent={{60,-90},{40,-70}})));
equation
  xi = h * sqrt(mu_0*abs(omega)/2/rho);
  kR = if xi > eps then xi * (sinh(2*xi)+sin(2*xi))/(cosh(2*xi)-cos(2*xi)) else 1;
  kX = if xi > eps then 3 / (2*xi) * (sinh(2*xi)-sin(2*xi))/(cosh(2*xi)-cos(2*xi)) else 1;

  connect(thermalCollector.port_a, resistor.heatPort) annotation (Line(
      points={{-40,-70},{-40,-50},{-30,-50}}, color={191,0,0}));
  connect(thermalCollector.port_b, heatPortWinding) annotation (Line(
      points={{-40,-90},{-40,-100},{0,-100}}, color={191,0,0}));
  connect(port_p, electroMagneticConverter.port_p) annotation (Line(
      points={{-100,0},{-10,0}}, color={255,170,85}));
  connect(electroMagneticConverter.port_n, port_n) annotation (Line(
      points={{10,0},{100,0}}, color={255,170,85}));
  connect(star.pin_n, ground.pin) annotation (Line(
      points={{70,-20},{76,-20},{80,-20}}, color={85,170,255}));
  connect(electroMagneticConverter.plug_p, resistor.plug_p) annotation (
      Line(
      points={{-10,-20},{-20,-20},{-20,-40}}, color={85,170,255}));
  connect(electroMagneticConverter.plug_n, multiStar.plug_p) annotation (
      Line(points={{10,-20},{15,-20},{20,-20}}, color={85,170,255}));
  connect(multiStar.starpoints, star.plug_p) annotation (Line(points={{40,
          -20},{45,-20},{50,-20}}, color={85,170,255}));
  connect(resistor.plug_n, inductor.plug_p)
    annotation (Line(points={{-20,-60},{-10,-60}}, color={85,170,255}));
  connect(inductor.plug_n, multiStar.plug_p)
    annotation (Line(points={{10,-60},{20,-60},{20,-20}}, color={85,170,255}));
  connect(kR_Expression.y, resistor.R_ref)
    annotation (Line(points={{39,-50},{-8,-50}}, color={0,0,127}));
  connect(kX_Expression.y, inductor.L)
    annotation (Line(points={{39,-80},{0,-80},{0,-72}}, color={0,0,127}));
  annotation (defaultComponentName="cage",
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics={Ellipse(
                extent={{-80,80},{80,-80}},
                fillColor={175,175,175},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{-20,76},{20,36}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{28,46},{68,6}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{28,-8},{68,-48}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{-20,-36},{20,-76}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{-68,-6},{-28,-46}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{-66,50},{-26,10}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Line(points={{-80,0},{-100,
          0}}, color={255,170,85}),Line(points={{100,0},{80,0}}, color={
          255,128,0}),
        Text(
          extent={{150,140},{-150,100}},
          textColor={0,0,255},
          textString="%name")}),
    Documentation(info="<html>
<div>
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/Machines/Components/rotorcage.png\">
</div>

<p>
The symmetric rotor cage model of this library does not consist of rotor bars and end rings. Instead the symmetric cage is modeled by an equivalent symmetrical winding. The rotor cage model consists of
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/m.png\"> phases. If the cage is modeled by equivalent stator winding parameters, the number of effective turns,
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/effectiveTurns.png\">, has to be chosen equivalent to the effective number of stator turns.
</p>

<h4>See also</h4>
<p>
<a href=\"modelica://Modelica.Magnetic.QuasiStatic.FundamentalWave.BasicMachines.Components.SaliencyCageWinding\">
SaliencyCageWinding</a>,
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.SymmetricPolyphaseCageWinding\">
Magnetic.FundamentalWave.BasicMachines.Components.SymmetricPolyphaseCageWinding</a>
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.RotorSaliencyAirGap\">
Magnetic.FundamentalWave.BasicMachines.Components.RotorSaliencyAirGap</a>
</p>
</html>"));
end SymmetricPolyphaseCageWinding;
