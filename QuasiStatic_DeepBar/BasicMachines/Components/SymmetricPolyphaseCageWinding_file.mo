within QuasiStatic_DeepBar.BasicMachines.Components;
model SymmetricPolyphaseCageWinding_file
  "Symmetrical rotor cage, deep bar data read from file"
  import Modelica.Constants.pi;
  import Modelica.Constants.eps;
  import Modelica.Constants.mu_0;
  extends
    Modelica.Magnetic.QuasiStatic.FundamentalWave.Interfaces.TwoPortExtended;
  parameter String fileName = Modelica.Utilities.Files.loadResource("modelica://QuasiStatic_DeepBar/Resources/deepbar/standard.txt");
  parameter Integer m=3 "Number of phases" annotation(Evaluate=true);
  parameter Boolean useHeatPort=false
    "Enable / disable (=fixed temperatures) thermal port"
    annotation (Evaluate=true);
  parameter Modelica.Units.SI.Resistance RRef
    "Winding resistance per phase at TRef, constant part";
  parameter Modelica.Units.SI.Resistance RRefvar
    "Winding resistance per phase at TRef, variable part due to deep bar effect";
  parameter Modelica.Units.SI.Temperature TRef(start=293.15)
    "Reference temperature of winding";
  parameter
    Modelica.Electrical.Machines.Thermal.LinearTemperatureCoefficient20
    alpha20(start=0) "Temperature coefficient of winding at 20 degC";
  final parameter Modelica.Units.SI.LinearTemperatureCoefficient alphaRef=
      Modelica.Electrical.Machines.Thermal.convertAlpha(
      alpha20,
      TRef,
      293.15) "Temperature coefficient of winding at reference temperature";
  parameter Modelica.Units.SI.Temperature TOperational(start=293.15)
    "Operational temperature of winding"
    annotation (Dialog(enable=not useHeatPort));
  parameter Modelica.Units.SI.Inductance Lsigma "Cage stray inductance, constant part";
  parameter Modelica.Units.SI.Inductance Lsigmavar "Cage stray inductance, variable part due to deep bar effect";
  parameter Real effectiveTurns=1 "Effective number of turns";

  final parameter Integer nBase=Modelica.Electrical.Polyphase.Functions.numberOfSymmetricBaseSystems(m)
    "Number of base systems";
  Modelica.Units.SI.ComplexCurrent i[m]=electroMagneticConverter.i
    "Cage currents";

  Modelica.Magnetic.QuasiStatic.FundamentalWave.Components.PolyphaseElectroMagneticConverter
    electroMagneticConverter(final m=m, final effectiveTurns=effectiveTurns)
    "Symmetric winding" annotation (Placement(transformation(
        origin={-60,-10},
        extent={{-10,-10},{10,10}},
        rotation=90)));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.VariableResistor
                                                           resistor(
    final useHeatPort=useHeatPort,
    final m=m,
    final T_ref=fill(TRef, m),
    final T=fill(TOperational, m),
    alpha_ref=fill(0, m))        annotation (Placement(transformation(
        origin={-80,-50},
        extent={{10,10},{-10,-10}},
        rotation=90)));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.Star star(final m=
        nBase)
           annotation (Placement(transformation(extent={{-10,-30},{10,-10}})));
  Modelica.Electrical.QuasiStatic.SinglePhase.Basic.Ground ground
    annotation (Placement(transformation(
        origin={30,-20},
        extent={{-10,10},{10,-10}},
        rotation=270)));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a heatPortWinding
 if useHeatPort "Heat ports of winding resistor"
    annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));
  Modelica.Thermal.HeatTransfer.Components.ThermalCollector
    thermalCollector(final m=m) if useHeatPort
    "Connector of thermal rotor resistance heat ports"
    annotation (Placement(transformation(extent={{-110,-90},{-90,-70}})));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.MultiStar multiStar(final m=m)
    annotation (Placement(transformation(extent={{-40,-30},{-20,-10}})));
  Modelica.Electrical.QuasiStatic.Polyphase.Basic.VariableInductor
                                                           inductor(m=m)
                                                                    annotation (Placement(transformation(
        origin={-60,-60},
        extent={{10,-10},{-10,10}},
        rotation=180)));
  Modelica.Blocks.Routing.Replicator replicatorR(final nout=m)
    annotation (Placement(transformation(extent={{0,-60},{-20,-40}})));
  Modelica.Blocks.Routing.Replicator replicatorL(final nout=m)
    annotation (Placement(transformation(extent={{0,-90},{-20,-70}})));
  Modelica.Blocks.Math.Add addR
    annotation (Placement(transformation(extent={{40,-60},{20,-40}})));
  Modelica.Blocks.Math.Add addL
    annotation (Placement(transformation(extent={{40,-90},{20,-70}})));
  Modelica.Blocks.Sources.Constant constR(k=RRef)
    annotation (Placement(transformation(extent={{100,-60},{80,-40}})));
  Modelica.Blocks.Sources.Constant constL(k=Lsigma)
    annotation (Placement(transformation(extent={{100,-90},{80,-70}})));
  Modelica.Blocks.Math.Gain varR(k=RRefvar)
                                          annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={30,40})));
  Modelica.Blocks.Math.Gain varL(k=Lsigmavar)  annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={30,80})));
  Modelica.Blocks.Tables.CombiTable1Ds combiTable1Ds(
    tableOnFile=true,
    tableName="deepbar",
    fileName=fileName,
    columns=2:3)
    annotation (Placement(transformation(extent={{-20,50},{0,70}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=omega)
    annotation (Placement(transformation(extent={{-60,50},{-40,70}})));
equation

  connect(thermalCollector.port_a, resistor.heatPort) annotation (Line(
      points={{-100,-70},{-100,-50},{-90,-50}},
                                              color={191,0,0}));
  connect(thermalCollector.port_b, heatPortWinding) annotation (Line(
      points={{-100,-90},{-100,-100},{0,-100}},
                                              color={191,0,0}));
  connect(port_p, electroMagneticConverter.port_p) annotation (Line(
      points={{-100,0},{-70,0}}, color={255,170,85}));
  connect(electroMagneticConverter.port_n, port_n) annotation (Line(
      points={{-50,0},{100,0}},color={255,170,85}));
  connect(star.pin_n, ground.pin) annotation (Line(
      points={{10,-20},{20,-20}},          color={85,170,255}));
  connect(electroMagneticConverter.plug_p, resistor.plug_p) annotation (
      Line(
      points={{-70,-20},{-80,-20},{-80,-40}}, color={85,170,255}));
  connect(electroMagneticConverter.plug_n, multiStar.plug_p) annotation (
      Line(points={{-50,-20},{-40,-20}},        color={85,170,255}));
  connect(multiStar.starpoints, star.plug_p) annotation (Line(points={{-20,-20},
          {-10,-20}},              color={85,170,255}));
  connect(resistor.plug_n, inductor.plug_p)
    annotation (Line(points={{-80,-60},{-70,-60}}, color={85,170,255}));
  connect(inductor.plug_n, multiStar.plug_p)
    annotation (Line(points={{-50,-60},{-40,-60},{-40,-20}},
                                                          color={85,170,255}));
  connect(replicatorR.y, resistor.R_ref)
    annotation (Line(points={{-21,-50},{-68,-50}}, color={0,0,127}));
  connect(replicatorL.y, inductor.L)
    annotation (Line(points={{-21,-80},{-60,-80},{-60,-72}}, color={0,0,127}));
  connect(addR.y, replicatorR.u)
    annotation (Line(points={{19,-50},{2,-50}}, color={0,0,127}));
  connect(addL.y, replicatorL.u)
    annotation (Line(points={{19,-80},{2,-80}}, color={0,0,127}));
  connect(constL.y, addL.u2) annotation (Line(points={{79,-80},{70,-80},{70,-86},
          {42,-86}}, color={0,0,127}));
  connect(constR.y, addR.u2) annotation (Line(points={{79,-50},{70,-50},{70,-56},
          {42,-56}}, color={0,0,127}));
  connect(realExpression.y, combiTable1Ds.u)
    annotation (Line(points={{-39,60},{-22,60},{-22,60}}, color={0,0,127}));
  connect(combiTable1Ds.y[1], varR.u) annotation (Line(points={{1,60},{10,60},{10,
          40},{18,40}}, color={0,0,127}));
  connect(combiTable1Ds.y[2], varL.u) annotation (Line(points={{1,60},{10,60},{10,
          80},{18,80}}, color={0,0,127}));
  connect(varR.y, addR.u1) annotation (Line(points={{41,40},{50,40},{50,-44},{42,
          -44}}, color={0,0,127}));
  connect(varL.y, addL.u1) annotation (Line(points={{41,80},{60,80},{60,-74},{42,
          -74}}, color={0,0,127}));
  annotation (defaultComponentName="cage",
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics={Ellipse(
                extent={{-80,80},{80,-80}},
                fillColor={175,175,175},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{-20,76},{20,36}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{28,46},{68,6}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{28,-8},{68,-48}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{-20,-36},{20,-76}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{-68,-6},{-28,-46}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Ellipse(
                extent={{-66,50},{-26,10}},
                fillColor={255,255,255},
                fillPattern=FillPattern.Solid),Line(points={{-80,0},{-100,
          0}}, color={255,170,85}),Line(points={{100,0},{80,0}}, color={
          255,128,0}),
        Text(
          extent={{150,140},{-150,100}},
          textColor={0,0,255},
          textString="%name")}),
    Documentation(info="<html>
<div>
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/Machines/Components/rotorcage.png\">
</div>

<p>
The symmetric rotor cage model of this library does not consist of rotor bars and end rings. Instead the symmetric cage is modeled by an equivalent symmetrical winding. The rotor cage model consists of
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/m.png\"> phases. If the cage is modeled by equivalent stator winding parameters, the number of effective turns,
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/effectiveTurns.png\">, has to be chosen equivalent to the effective number of stator turns.
</p>

<h4>See also</h4>
<p>
<a href=\"modelica://Modelica.Magnetic.QuasiStatic.FundamentalWave.BasicMachines.Components.SaliencyCageWinding\">
SaliencyCageWinding</a>,
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.SymmetricPolyphaseCageWinding\">
Magnetic.FundamentalWave.BasicMachines.Components.SymmetricPolyphaseCageWinding</a>
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.RotorSaliencyAirGap\">
Magnetic.FundamentalWave.BasicMachines.Components.RotorSaliencyAirGap</a>
</p>
</html>"));
end SymmetricPolyphaseCageWinding_file;
