using ElectricalEngineering, PyPlot
include("deepbar.jl")
mkpath("../deepbar")
close("all")

#######################################################################
# Frequency reange
f = 100
# Number of freuquency points (except DC)
F = 100
# Resistivity
rho = 3E-8


#######################################################################
### CASE 1: STANDARD BAR SHAPE
#######################################################################
# Inner radius of inner cage slot
ri=1.40E-3
# Outer radius of inner cage slot
ra=3E-3
# Radius of outer cage slot
ro=0
# Distance between centers of inner cage radii
hi=20.2E-3
# Height of intermediate segment between inner and outer cage slot
hs=0
# Width of intermediate segment between inner and outer cage slot
ws=0
# Number of layers
N=164

println("STANDARD")
hsum, w, y = doubleslot(ri,ra,ro,hi,hs,ws,N)
plotslot(w, y)
Rdc, Ldc, omega, kR, kX = deepbar(hsum, w, y, N, rho, f, F)
writedeepbar(Rdc, Ldc, omega, kR, kX, "../deepbar/standard.txt")
println("    lsigma = ",LdcRef/Ldc)

#######################################################################
### CASE 2: DOUBLE CAGE
#######################################################################
# Inner radius of inner cage slot
ri=1.40E-3
# Outer radius of inner cage slot
ra=2.70E-3
# Radius of outer cage slot
ro=2.50E-3
# Distance between centers of inner cage radii
hi=16.5E-3
# Height of intermediate segment between inner and outer cage slot
hs=2.8E-3
# Width of intermediate segment between inner and outer cage slot
ws=1.50E-3
# Number of layers
N=164

println("DOUBLECAGE")
hsum, w, y = doubleslot(ri,ra,ro,hi,hs,ws,N)
plotslot(w, y)
Rdc, Ldc, omega, kR, kX = deepbar(hsum, w, y, N, rho, f, F)
writedeepbar(Rdc, Ldc, omega, kR, kX, "../deepbar/doublecage.txt")
println("    lsigma = ",LdcRef/Ldc)

#######################################################################
### CASE 3: DEEP BAR 1
#######################################################################
# Inner radius of inner cage slot
ri=0
# Outer radius of inner cage slot
ra=0
# Radius of outer cage slot
ro=0
# Distance between centers of inner cage radii
hi=0
# Width of intermediate segment between inner and outer cage slot
ws=4.5E-3
# Height of intermediate segment between inner and outer cage slot
hs=0.000106058127339294 / ws
# Number of layers
N=164

println("DEEPBAR1")
hsum, w, y = doubleslot(ri,ra,ro,hi,hs,ws,N)
plotslot(w, y)
Rdc, Ldc, omega, kR, kX = deepbar(hsum, w, y, N, rho, f, F)
writedeepbar(Rdc, Ldc, omega, kR, kX, "../deepbar/deepbar1.txt")
println("    lsigma = ",LdcRef/Ldc)

#######################################################################
### CASE 4: DEEP BAR 2
#######################################################################
# Inner radius of inner cage slot
ri=0
# Outer radius of inner cage slot
ra=0
# Radius of outer cage slot
ro=0
# Distance between centers of inner cage radii
hi=0
# Width of intermediate segment between inner and outer cage slot
ws=2.7E-3
# Height of intermediate segment between inner and outer cage slot
hs=0.000106058127339294 / ws
# Number of layers
N=164

println("DEEPBAR2")
hsum, w, y = doubleslot(ri,ra,ro,hi,hs,ws,N)
plotslot(w, y)
Rdc, Ldc, omega, kR, kX = deepbar(hsum, w, y, N, rho, f, F)
writedeepbar(Rdc, Ldc, omega, kR, kX, "../deepbar/deepbar2.txt")
println("    lsigma = ",LdcRef/Ldc)
