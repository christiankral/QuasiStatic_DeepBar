using ElectricalEngineering, PyPlot
# Permeability of vacuum
const mu0 = pi * 4E-7

function doubleslot(ri,ra,ro,hi,hs,ws,N)
  # Init width, height and x vector
  w = zeros(N)
  h = zeros(N)
  x = zeros(N)
  # Total height of double slot
  hsum = ri + hi + ra + hs + 2 * ro
  # Absolute slot coordinate
  y = hsum * collect(0.5:1:N)/N

  # Assemble different sections, starting inside => out
  # Inner radius of inner cage
  i1 = (0 .<= y) .&& (y .< ri)
  x[i1] = y[i1] .- ri
  w[i1] = 2 * sqrt.(ri^2 .- x[i1].^2)
  # Linear range between inner and outer circle of inner cage
  i2 = (ri .<= y) .&& (y .<= ri + hi)
  x[i2] = y[i2] .- ri
  w[i2] = 2 * (ri .+ (ra - ri) * x[i2] / hi)
  #  Outer circle of inner cage
  i3 = (ri + hi .<= y) .&& (y .< ri + hi + ra)
  x[i3] = y[i3] .- ri .- hi
  w[i3] = 2 * sqrt.(ra^2 .- x[i3].^2)
  # Intermediate segment between inner and outer cage
  i4 = (ri + hi + ra .<= y) .&& (y .< ri + hi + ra + hs)
  w[i4] .= ws
  # w[i4] = fill(ws,length(w[i4]))
  # Outer cage
  i5 = (ri + hi + ra + hs .<= y) .&& (y .< ri + hi + ra + hs + 2 * ro)
  x[i5] = y[i5] .- ri .- hi .- ra .- hs .- ro
  w[i5] = 2 * sqrt.(ro^2 .- x[i5].^2)
  return hsum,w,y
end

function plotslot(w,y)
  figure()
  plot(w/2,y,color=colorBlack1)
  plot(-w/2,y,color=colorBlack1)
  axis("square")
end

function deepbar(hsum, w, y, N, rho, f, F)
  # hsum = Total height of slot
  # w    = Width vector of slot geometry
  # y    = Radial coodinate of slot geometry from inside => outside
  # N    = Number of slot layers
  # f    = Maximum Freuquency to be caclulated
  # F    = Number of frequency calculations


  # Height of one slot layer
  dh = hsum / N
  # Cumulative area of cross section of layers from bottom => top
  A = cumsum(w) * dh
  # Inductances per axial length of layers
  L = mu0 * dh ./ w
  # Resistances per axial length of layers
  R = rho / dh ./ w

  # Initialize frequency dependent impedance, resistance and inductance vector
  # per axial length
  Rf = zeros(F+1)
  Lf = zeros(F+1)
  Zf = fill(0.0+0.0im,F+1)

  # DC resistance
  Rf[1] = 1 ./ sum(1 ./ R)
  Lf[1] = sum(L .* A.^2) ./ A[end]^2
  Zf[1] = Rf[1]

  # Angular frequency vector
  omega = 2 * pi * collect(0:f/F:f)

  # Lattice network calculations
  for i in collect(2:F+1)
    Zf[i] = R[1] + j * omega[i] * L[1]
    for k in collect(2:N)
      Zf[i] = ∥(Zf[i],R[k]) + j*omega[i]*L[k]
    end
  end
  # Determine resistance and inductance from impedance
  iac = collect(2:F+1)
  Rf[iac] = real(Zf[iac])
  Lf[iac] = imag(Zf[iac]) ./ omega[iac]

  # Resistive and inductive deep bar factors
  kR = Rf / Rf[1]
  kX = Lf / Lf[1]

  println("    A (m2) = ", A[end])
  println("    Rdc (Ohm) = ", Rf[1])
  println("    Ldc (H) = ", Lf[1])

  return Rf[1], Lf[1], omega, kR, kX
end

function writedeepbar(Rdc, Ldc, omega, kR, kX, fileName)
  M = length(omega)
  io = open(fileName, "w")
  write(io, "#1\n")
  write(io, "# Rdc (Ohm) = " * string(Rdc) * "\n")
  write(io, "# Ldc (H) = " * string(Ldc) * "\n")
  write(io, "# omega, kR, kX\n")
  write(io, "double deepbar(" * string(M) * ",3)\n")
  for k in collect(1:M)
    write(io, string(omega[k]), " ", string(kR[k]), " ", string(kX[k]), "\n")
  end
  close(io)
end
