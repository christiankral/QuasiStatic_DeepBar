within ;
package QuasiStatic_DeepBar "Quasi-static electric machine models based on deep bar effect"
extends Modelica.Icons.Package;

annotation (version="0.X.X", versionDate="2022-XX-XX",uses(Modelica(version="4.0.0"), Complex(version="4.0.0")));
end QuasiStatic_DeepBar;
